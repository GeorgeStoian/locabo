/**
 * Created by George on 18/03/2017.
 */

'use strict';

console.log('Loading function');

let moment = require('moment');
let uuid = require('node-uuid');
let _ = require('underscore');
let AWS = require('aws-sdk');
let creds = new AWS.EnvironmentCredentials('AWS');

// Lambda provided credentials
const dynamoConfig = {
  credentials: creds,
  region: process.env.AWS_REGION
};
const docClient = new AWS.DynamoDB.DocumentClient(dynamoConfig);
const ddbTableSaved = 'SavedOpportunities';
const ddbTableOpportunities = 'Opportunities';


/**
 * Provide an event that contains the following keys:
 *
 * - resource: API Gateway resource for event
 * - path: path of the HTTPS request to the microservices API call
 * - httpMethod: HTTP method of the HTTPS request from microservices API call
 * - headers: HTTP headers for the HTTPS request from microservices API call
 * - queryStringParameters: query parameters of the HTTPS request from microservices API call
 * - pathParameters: path parameters of the HTTPS request from microservices API call
 * - stageVariables: API Gateway stage variables, if applicable
 * - body: body of the HTTPS request from the microservices API call
 */
module.exports.handler = function (event, context, callback) {
  console.log(event);
  let _response = "";
  let invalid_path_err = {"Error": "Invalid path request " + event.resource + ', ' + event.httpMethod};

  //Saved opportunities routes
  if (event.resource === '/saved-opportunities' && event.httpMethod === "GET") {
    console.log("listing the saved opportunities for a user");

    var params = {
      TableName: ddbTableSaved,
      KeyConditionExpression: 'userId = :uid',
      ExpressionAttributeValues: {
        ':uid': event.queryStringParameters.userId
      }
    };

    docClient.query(params, function (err, resp) {
      if (err) {
        console.log(err);
        _response = buildOutput(500, err);
        return callback(_response, null);
      }
      if (resp.Items) {
        resp.Count = resp.Items.length;
      }
      else {
        resp.Count = 0;
      }
      _response = buildOutput(200, resp);
      return callback(null, _response);
    });
  }

  //Opportunities routes
  else if (event.resource === '/opportunities' && event.httpMethod === "GET") {
    console.log("listing all the opportunities");

    var params = {
      TableName: ddbTableOpportunities
    };

    docClient.scan(params, function (err, resp) {
      if (err) {
        console.log(err);
        _response = buildOutput(500, err);
        return callback(_response, null);
      }
      if (resp.Items) {
        resp.Count = resp.Items.length;
      }
      else {
        resp.Count = 0;
      }
      _response = buildOutput(200, resp);
      return callback(null, _response);
    });
  }
  else if (event.resource === '/opportunities/{opportunityId}' && event.httpMethod === "POST") {
    console.log("Creating a new opportunity");

    let opportunity = JSON.parse(event.body);
    //create unique opportunityId for the new opportunity
    opportunity.opportunityId = uuid.v4();
    //set the created datetime stamp for the new opportunity
    opportunity.createdAt = moment().utc().format();
    let params = {
      TableName: ddbTableOpportunities,
      Item: opportunity
    };
    docClient.put(params, function (err, data) {
      if (err) {
        console.log(err);
        _response = buildOutput(500, err);
        return callback(_response, null);
      }
      _response = buildOutput(200, opportunity);
      return callback(null, _response);
    });
  }
  else if (event.resource === '/opportunities/{opportunityId}' && event.httpMethod === "PUT") {
    console.log("updating an opportunity");
    let opportunity = JSON.parse(event.body);
    let params = {
      TableName: ddbTableOpportunities,
      Item: opportunity
    };
    docClient.put(params, function (err, data) {
      if (err) {
        console.log(err);
        _response = buildOutput(500, err);
        return callback(_response, null);
      }
      _response = buildOutput(200, opportunity);
      return callback(null, _response);
    });
  }
  else if (event.resource === '/opportunities/{opportunityId}' && event.httpMethod === "DELETE") {
    console.log("delete an opportunity");
    let params = {
      TableName: ddbTableOpportunities,
      Key: {
        opportunityId: event.pathParameters.opportunityId
      }
    };
    docClient.delete (params, function (err, data) {
      if (err) {
        console.log(err);
        _response = buildOutput(500, err);
        return callback(_response, null);
      }
      _response = buildOutput(200, data);
      return callback(null, _response);
    });
  }
  else if (event.resource === '/opportunities/{opportunityId}' && event.httpMethod === "GET") {
    console.log("get an opportunity");
    let params = {
      TableName: ddbTableOpportunities,
      Key: {
        opportunityId: event.pathParameters.opportunityId
      }
    };
    docClient.get(params, function (err, data) {
      if (err) {
        console.log(err);
        _response = buildOutput(500, err);
        return callback(_response, null);
      }
      _response = buildOutput(200, data);
      return callback(null, _response);
    });
  }
  else {
    _response = buildOutput(500, invalid_path_err);
    return callback(_response, null);
  }
};

/* Utility function to build HTTP response for the microservices output */
function buildOutput(statusCode, data) {
  let _response = {
    statusCode: statusCode,
    headers: {
      "Access-Control-Allow-Origin": "*"
    },
    body: JSON.stringify(data)
  };
  return _response;
};
