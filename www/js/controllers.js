angular.module('locabo.controllers', ['locabo.service.auth'])

.controller('SignInCtrl', function($scope, $state, $ionicHistory, $ionicLoading, $location, $ionicPopup, authService) {

  var alertPopup;
  $scope.login = function(user, isValid) {
    console.log('Login ', user);
    if (isValid) {
      $ionicLoading.show();
      authService.signin(user).then(function(result) {
        console.log(result);
        console.log('access token + ' + result.getIdToken().getJwtToken());
        $ionicLoading.hide();
        $state.go('tab.map', {});
      }, function(msg) {
        console.log(msg);
        alertPopup = $ionicPopup.alert({
          title: 'Sign in failed!',
          template: 'Please check your credentials!'
        });
        $ionicLoading.hide();
        if ($scope.$$phase != '$digest') {
          $scope.$apply();
        }
        return;
      });
    } else {
      alertPopup = $ionicPopup.alert({
        title: 'Sign in failed!',
        template: 'Please make sure you filled in all the fields correctly!'
      });
      $ionicLoading.hide();
    }
  };

  $scope.goGuest = function () {
    console.log("Logging in as guest");
    $state.go('tab.map', {});
  };
})

.controller('SignUpCtrl', function($scope, $state, $ionicHistory, $ionicLoading, $location, $ionicPopup, authService) {
  $scope.errormessage = "";
  var alertPopup;
  $scope.register = function(newuser, isValid) {
    console.log(newuser);

    if (isValid) {
      $ionicLoading.show();
      authService.signup(newuser).then(function() {
        $ionicLoading.hide();
        $state.go('tab.map', {});
      }, function(msg) {
        if (msg.match(/User already exists/g)){
          alertPopup = $ionicPopup.alert({
            title: 'Sign up failed!',
            template: 'Email address is already registered. Try signing in!'
          });
        }
        $ionicLoading.hide();
        if ($scope.$$phase != '$digest') {
          $scope.$apply();
        }
        return;
      });

    } else {
      alertPopup = $ionicPopup.alert({
        title: 'Sign up failed!',
        template: 'Please make sure you filled in all the fields correctly!'
      });
      $ionicLoading.hide();
      if ($scope.$$phase != '$digest') {
        $scope.$apply();
      }
    }
  }
})

.controller('ForgotPasswordCtrl', function($scope) {})

.controller('MapCtrl', function($scope, $ionicLoading, Markers) {
  console.log("MapCtrl");
  $scope.initialise = function () {
    $scope.map = Markers.all();
  };
  google.maps.event.addDomListener(document.getElementById("map"), 'load', $scope.initialise());
})

.controller('AboutCtrl', function($scope) {
  $scope.version = APP_VERSION;
})

.controller('SavedCtrl', function($scope, SavedOpportunities) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.opportunities = SavedOpportunities.all();
  $scope.remove = function(opportunity) {
    SavedOpportunities.remove(opportunity);
  };
})

.controller('TrendingCtrl', function($scope, $ionicLoading, $ionicPopup, Opportunities) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  var alertPopup;
  var _filter = "";

  $scope.opportunities = [];

  // A utitlity function for loading all opportunities
  var loadOpportunities = function() {
    $ionicLoading.show();

    Opportunities.listOpportunities(_filter, function(err, opportunities) {
      if (err) {
        console.log(err);
        $ionicLoading.hide();
        alertPopup = $ionicPopup.alert({
          title: 'Retreive failed!',
          template: 'Please make sure you are connected to the Internet!'
        });
        return;
      }
      $scope.opportunities = opportunities;
      console.log(opportunities);
      $ionicLoading.hide();
    });
  };
  loadOpportunities();
})

.controller('')

.controller('SavedDetailCtrl', function($scope, $stateParams, SavedOpportunities) {
  $scope.opportunity = SavedOpportunities.get($stateParams.opportunityId);
})

.controller('TrendingDetailCtrl', function($scope, $stateParams, $ionicLoading, $ionicPopup, Opportunities) {

  var alertPopup;
  var loadOpportunity = function (){
    $ionicLoading.show();
    Opportunities.getOpportunity($stateParams.opportunityId, function (err, opportunity) {
      if (err) {
        console.log('some error');
        console.log(err);
        $ionicLoading.hide();
        alertPopup = $ionicPopup.alert({
          title: 'Retreive failed!',
          template: 'Please make sure you are connected to the Internet!'
        });
        return;
      }
      //Call returns an array with one result in so pass that back
      console.log(opportunity[0]);
      $scope.opportunity = opportunity[0];
      $ionicLoading.hide();
    });
  };
  $scope.saveOpportunity = function(opportunity){
    alertPopup = $ionicPopup.alert({
      title: 'Opportunity saved!',
      template: 'You are now ready to experience awesome!'
    });
  };
  loadOpportunity();
})

.controller('AccountCtrl', function($scope, $state, $ionicHistory, $ionicLoading, $ionicPopup, authService) {
  var alertPopup;
  $scope.logout = function() {
    $ionicLoading.show();
    authService.logOut().then(function(){

      $ionicLoading.hide();
      $state.go('signIn', {});
    }, function(err){
      console.log(err);
      alertPopup = $ionicPopup.alert({
        title: 'Sign out failed!',
        template: 'There was a problem signing out. Please try again!'
      });
      $ionicLoading.hide();
    });
  };

  authService.getUserInfo().then(function(userinfo) {
    $scope.version = APP_VERSION;
    $scope.userinfo = userinfo;
  }, function(msg) {
    console.log(msg);
  });
});
