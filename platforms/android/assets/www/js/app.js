// Locabo App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'locabo' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('locabo', ['ionic', 'ngMessages', 'angularjs-crypto', 'locabo.controllers', 'locabo.services', 'locabo.service.auth'])


.constant('$ionicLoadingConfig', {
  template: '<ion-spinner icon="bubbles" class="spinner-royal"></ion-spinner>'
})

.run(function($ionicPlatform, $rootScope, $state, $localstorage, authService) {
  console.log('In run at the top');
  $rootScope.base64Key = CryptoJS.enc.Base64.parse("2b7e151738aed2a6abf7158809cf4f3c");
  $rootScope.iv = CryptoJS.enc.Base64.parse("3ad77bb90d7a3770a89ecaf32466ef97");
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    var savedEmail = $localstorage.get('username');
    var savedPass = $localstorage.get('password');
    if (savedEmail && savedPass){
      //Decrypt the saved password
      var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(savedPass)});
      var decryptedPassword = CryptoJS.AES.decrypt(cipherParams,$rootScope.base64Key,{ iv: $rootScope.iv });
      decryptedPassword = decryptedPassword.toString(CryptoJS.enc.Utf8);
      console.log('decryptedPassword ' + decryptedPassword);
      var savedUser = {email:savedEmail, password:decryptedPassword};
      authService.signin(savedUser).then(function(result) {
        console.log(result);
        $state.go('tab.map', {});
      }, function(msg) {
        console.log(msg);
      });
    }


  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  $ionicConfigProvider.tabs.position('bottom');
  $ionicConfigProvider.backButton.previousTitleText(false);
  $ionicConfigProvider.views.swipeBackEnabled(true);
  $ionicConfigProvider.navBar.alignTitle("center");
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  //Sing-in page
  .state('signIn', {
    url: '/sign-in',
    templateUrl: 'templates/sign-in.html',
    controller: 'SignInCtrl'
  })
  .state('forgotPassword', {
    url: '/forgot-password',
    templateUrl: 'templates/forgot-password.html',
    controller: 'ForgotPasswordCtrl'
  })
  .state('signUp', {
  url: '/sign-up',
  templateUrl: 'templates/sign-up.html',
  controller: 'SignUpCtrl'
  })
  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })
  .state('tab.saved', {
    url: '/saved',
    views: {
      'tab-saved': {
        templateUrl: 'templates/tab-saved.html',
        controller: 'SavedCtrl'
      }
    }
  })
  .state('tab.saved-detail', {
    url: '/saved/:opportunityId',
    views: {
      'tab-saved': {
        templateUrl: 'templates/saved-detail.html',
        controller: 'SavedDetailCtrl'
      }
    }
  })
  .state('tab.map', {
    url: '/map',
    views: {
      'tab-map': {
        templateUrl: 'templates/tab-map.html',
        controller: 'MapCtrl'
      }
    }
  })
  .state('tab.trending', {
    url: '/trending',
    views: {
      'tab-trending': {
        templateUrl: 'templates/tab-trending.html',
        controller: 'TrendingCtrl'
      }
    }
  })
  .state('tab.trending-detail', {
    url: '/trending/:opportunityId',
    views: {
      'tab-trending': {
        templateUrl: 'templates/trending-detail.html',
        controller: 'TrendingDetailCtrl'
      }
    }
  })
  .state('tab.about', {
    url: '/about',
    views: {
      'tab-about': {
        templateUrl: 'templates/tab-about.html',
        controller: 'AboutCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/sign-in');
})

.run(function($rootScope, $state, authService, cfCryptoHttpInterceptor) {
  console.log('In run');
  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
    if (toState.authenticate) {
      authService.isAuthenticated().then(function(authenticated) {
        if (!authenticated) {
          // User isn’t authenticated
          $state.transitionTo("signin");
          event.preventDefault();
        }
      }).catch(function(result) {
        // User isn’t authenticated
        $state.transitionTo("signin");
        event.preventDefault();
      });
    }
  });
});
