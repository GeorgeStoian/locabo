angular.module('locabo.services', [])

.factory('Opportunities', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var opportunities = [{
    id: 0,
    name: 'Happy Hour',
    what: 'Come and enjoy reduced prices on food and drinks!',
    when: '12 till 2pm every Wednesday',
    where: '16 Castle St, L2 0NE',
    amount: 'unlimited',
    face: 'img/pinch.png'
  }, {
    id: 1,
    name: 'Tea Making Class',
    what: 'Fancy drinking exotic tea? Come and enjoy one on the house AND we teach you how to best make it!',
    when: '1 till 2pm on Wednesday',
    where: '65-67 Bold St, L1 4EZ',
    amount: '13',
    face: 'img/leaf.jpg'
  }, {
    id: 2,
    name: '2for1 coffees',
    what: 'Come and enjoy the best coffee and the best prices!',
    when: '9 till 11am every Monday',
    where: '16 Castle St, L2 0NE',
    amount: 'unlimited',
    face: 'img/pinch.png'
  }, {
    id: 3,
    name: 'Make your own cocktail class',
    what: 'Claim your FREE Cocktail and wow your friends by learning how to make it!',
    when: '2 till 2pm today',
    where: '10 Castle St, L2 0NE',
    amount:  '8',
    face: 'img/McGuffies.png'
  }, {
    id: 4,
    name: 'Bring a friend get 50% off',
    what: 'Bring a friend along and you both get 50% off on drinks!',
    when: '4 till 6pm every Friday',
    where: '10 Castle St, L2 0NE',
    amount: 'unlimited',
    face: 'img/McGuffies.png'
  }];

  return {
    all: function() {
      return opportunities;
    },
    remove: function(opportunity) {
      opportunities.splice(opportunities.indexOf(opportunity), 1);
    },
    get: function(opportunityId) {
      for (var i = 0; i < opportunities.length; i++) {
        if (opportunities[i].id === parseInt(opportunityId)) {
          return opportunities[i];
        }
      }
      return null;
    }
  };
})

.factory('SavedOpportunities', function() {
  // Might use a resource here that returns a JSON array
  // Some fake testing data
  var opportunities = [{
    id: 0,
    name: 'Happy Hour',
    what: 'Come and enjoy reduced prices on food and drinks!',
    when: '12 till 2pm every Wednesday',
    where: '16 Castle St, L2 0NE',
    amount:  'unlimited',
    face: 'img/pinch.png'
  }, {
    id: 1,
    name: 'Make your own cocktail class',
    what: 'Claim your FREE Cocktail and wow your friends by learning how to make it!',
    when: '2 till 2pm today',
    where: '10 Castle St, L2 0NE',
    amount:  '8',
    face: 'img/McGuffies.png'
  }];

  return {
    all: function() {
      return opportunities;
    },
    remove: function(opportunity) {
      opportunities.splice(opportunities.indexOf(opportunity), 1);
    },
    get: function(opportunityId) {
      for (var i = 0; i < opportunities.length; i++) {
        if (opportunities[i].id === parseInt(opportunityId)) {
          return opportunities[i];
        }
      }
      return null;
    }
  };
})

.factory('Markers', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var contentString = '<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<img src="./img/pinch.png" style="display:block;margin-right:auto;margin-left:auto">'+
    '<div id="bodyContent">'+
    '<p style="font-family:locaboFont;font-size:large;"><b>Make your own Cocktail Class</b></p>'+
    '<p style="font-family:locaboFont;text-align:center;font-size:smaller;">Claim your FREE Cocktail and wow your friends<br> by learning how to make it!</p>'+
    '<p style="font-family:locaboFont;text-align:center;font-size:large;"><b>From 2 till 2:30pm today!</b></p>'+
    '<p style="font-family:locaboFont;text-align:center;font-size:larger">8/10 Left!</p>'+
    '</div>'+
    '</div>';
  var infowindow;

  var opportunities = [
    ['Opportunity 1', 51.026447, -1.391449, 4],
    ['Opportunity 2', 51.026647, -1.391249, 5],
    ['Opportunity 3', 53.405581, -2.988847, 3],
    ['Opportunity 4', 53.407103, -2.986358, 2],
    ['Opportunity 5', 53.406103, -2.987358, 1]
  ];

  //Initialize the map
  console.log("In Google.maps.event.addDomListener");
  var mapOptions = {
    zoom: 17,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true,
    styles: [
      {
        featureType: 'poi',
        elementType: 'labels.text',
        stylers: [{visibility: 'off'}]
      }
    ]
  };

  var map = new google.maps.Map(document.getElementById("map"), mapOptions);

  navigator.geolocation.getCurrentPosition(function (pos) {
    map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
    var myLocation = new google.maps.Marker({
      position: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),
      map: map,
      title: "My Location"
    });
  });

  infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  // Adds markers to the map.

  // Marker sizes are expressed as a Size of X,Y where the origin of the image
  // (0,0) is located in the top left of the image.

  // Origins, anchor positions and coordinates of the marker increase in the X
  // direction to the right and in the Y direction down.
  var image = {
    url: './img/locabo.png',
    // This marker is 20 pixels wide by 32 pixels high.
    size: new google.maps.Size(20, 32),
    // The origin for this image is (0, 0).
    origin: new google.maps.Point(0, 0),
    // The anchor for this image is the base of the flagpole at (0, 32).
    anchor: new google.maps.Point(10, 32)
  };
  // Shapes define the clickable region of the icon. The type defines an HTML
  // <area> element 'poly' which traces out a polygon as a series of X,Y points.
  // The final coordinate closes the poly by connecting to the first coordinate.
  var shape = {
    coords: [1, 1, 1, 20, 18, 20, 18, 1],
    type: 'poly'
  };
  for (var i = 0; i < opportunities.length; i++) {
    var opportunity = opportunities[i];
    var marker = new google.maps.Marker({
      position: {lat: opportunity[1], lng: opportunity[2]},
      map: map,
      icon: image,
      shape: shape,
      title: opportunity[0],
      zIndex: opportunity[3]
    });
    google.maps.event.addListener(marker, 'click', function() {
      var marker_map = this.getMap();
      infowindow.open(marker_map,this);
    });
  }

  return {
    all: function() {
      return map;
    }
  };
});
