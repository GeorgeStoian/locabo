angular.module('locabo.service.auth', ['locabo.utils', 'angularjs-crypto'])
    .service('authService', function($q, $_, $localstorage, $window, $ionicHistory, $rootScope) {

        var _this = this;
        this.signup = function(newuser) {
            var deferred = $q.defer();

            var user = {
                email: newuser.email,
                password: newuser.password
            };

            newuser.username = newuser.email.replace("@", "_").replace(".", "_");

            var poolData = {
                UserPoolId: YOUR_USER_POOL_ID,
                ClientId: YOUR_USER_POOL_CLIENT_ID,
                Paranoia: 8
            };
            var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);

            var attributeList = [];

            var dataEmail = {
                Name: 'email',
                Value: newuser.email
            };

            var dataName = {
                Name: 'name',
                Value: newuser.name
            };

            var attributeEmail = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataEmail);
            var attributeName = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataName);

            attributeList.push(attributeEmail);
            attributeList.push(attributeName);

            console.log("Submitting " + newuser.name);
            userPool.signUp(newuser.username, newuser.password, attributeList, null, function(err, result) {
                if (err) {
                    console.log(err);
                    deferred.reject(err.message);
                } else {
                    console.log('Successfully signed up new user ' + newuser.email);
                    console.log('Signing in new user ' + newuser.email);

                    _this.signin(user).then(function(result) {
                        console.log('Return overall result of signup function');
                        deferred.resolve(result);
                    }, function (err){
                        console.log(err);
                        deferred.reject(err.message);
                    });
                }
            });
            return deferred.promise;
        };

        this.signin = function(user) {
            var deferred = $q.defer();

            var authenticationData = {
                Username: user.email,
                Password: user.password
            };

            var authenticationDetails = new AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);
            var poolData = {
                UserPoolId: YOUR_USER_POOL_ID,
                ClientId: YOUR_USER_POOL_CLIENT_ID,
                Paranoia: 8
            };

            var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
            var userData = {
                Username: user.email,
                Pool: userPool
            };

            var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);

            try {
                cognitoUser.authenticateUser(authenticationDetails, {
                    onSuccess: function(result) {
                        console.log("Signin successful for user " + cognitoUser.username);
                        $localstorage.set('username', user.email);
                        //encrypt the password and save it in localstorage
                        var encryptedPassword = CryptoJS.AES.encrypt(user.password, $rootScope.base64Key, { iv: $rootScope.iv });
                        var ciphertext = encryptedPassword.ciphertext.toString(CryptoJS.enc.Base64);
                        $localstorage.set('password', ciphertext);
                        deferred.resolve(result);
                    },
                    onFailure: function(err) {
                        deferred.reject(err);
                    }
                });
            } catch (e) {
                console.log("Signin failed with error: " + e);
                deferred.reject(e);
            }
            return deferred.promise;
        };

        this.isAuthenticated = function() {
            var deferred = $q.defer();
            var data = {
                UserPoolId: YOUR_USER_POOL_ID,
                ClientId: YOUR_USER_POOL_CLIENT_ID,
                Paranoia: 8
            };
            var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(data);
            var cognitoUser = userPool.getCurrentUser();

            try {
                if (cognitoUser != null) {
                    cognitoUser.getSession(function(err, session) {
                        if (err) {
                            deferred.resolve(false);
                        }
                        deferred.resolve(true);
                    });
                } else {
                    deferred.resolve(false);
                }
            } catch (e) {
                console.log(e);
                deferred.resolve(false);
            }

            return deferred.promise;

        };

        this.logOut = function() {
            var deferred = $q.defer();
            try {
                var data = {
                    UserPoolId: YOUR_USER_POOL_ID,
                    ClientId: YOUR_USER_POOL_CLIENT_ID,
                    Paranoia: 8
                };
                var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(data);
                var cognitoUser = userPool.getCurrentUser();
                console.log('Current user is ' + cognitoUser.username);
                if (cognitoUser != null) {
                    console.log('Signing them out');
                    console.log(typeof cognitoUser.signOut);
                    console.log(cognitoUser);
                    cognitoUser.signOut();
                    $window.localStorage.clear();
                    $ionicHistory.clearCache();
                    $ionicHistory.clearHistory();
                }
                deferred.resolve();
            } catch (e) {
                console.log(e);
                deferred.reject(e);
            }
            return deferred.promise;
        };

        this.getUserAccessToken = function() {
            var deferred = $q.defer();

            var data = {
                UserPoolId: YOUR_USER_POOL_ID,
                ClientId: YOUR_USER_POOL_CLIENT_ID,
                Paranoia: 8
            };

            var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(data);
            var cognitoUser = userPool.getCurrentUser();

            if (cognitoUser != null) {

                cognitoUser.getSession(function(err, session) {
                    if (err) {
                        console.log(err);
                        deferred.reject(err);
                    }
                    deferred.resolve(session.idToken);
                });

            } else {
                deferred.reject();
            }

            return deferred.promise;
        };

        this.getUserAccessTokenWithUsername = function() {
            var deferred = $q.defer();

            var data = {
                UserPoolId: YOUR_USER_POOL_ID,
                ClientId: YOUR_USER_POOL_CLIENT_ID,
                Paranoia: 8
            };

            var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(data);
            var cognitoUser = userPool.getCurrentUser();

            if (cognitoUser != null) {

                cognitoUser.getSession(function(err, session) {
                    if (err) {
                        console.log(err);
                        deferred.reject(err);
                    }
                    deferred.resolve({
                        token: session.idToken,
                        username: cognitoUser.username
                    });
                });

            } else {
                deferred.reject();
            }

            return deferred.promise;
        };

        this.getUserInfo = function() {
            var deferred = $q.defer();

            var userinfo = {
                email: "",
                name: "",
                username: ""
            };
            var data = {
                UserPoolId: YOUR_USER_POOL_ID,
                ClientId: YOUR_USER_POOL_CLIENT_ID,
                Paranoia: 8
            };

            var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(data);
            var cognitoUser = userPool.getCurrentUser();

            if (cognitoUser != null) {

                cognitoUser.getSession(function(err, session) {
                    if (err) {
                        console.log(err);
                        deferred.reject(err);
                    }

                    cognitoUser.getUserAttributes(function(err, result) {
                        if (err) {
                            console.log(err);
                            deferred.reject(err);
                        }

                        var nm = $_.where(result, {
                            Name: "name"
                        });
                        if (nm.length > 0) {
                            userinfo.name = nm[0].Value;
                        }

                        var em = $_.where(result, {
                            Name: "email"
                        });
                        if (em.length > 0) {
                            userinfo.email = em[0].Value;
                        }

                        userinfo.username = cognitoUser.getUsername();

                        deferred.resolve(userinfo);

                    });
                });
            } else {
                deferred.reject();
            }

            return deferred.promise;

        }

    });
